import os
import re
import pandas

class Student:

    def __init__(self, entry):
        self.last_name  = entry['Last Name']
        self.first_name = entry['First Name']
        self.email      = entry['Email ID']
        self.unid       = self.email[0:8]
        self.raw_id     = self.unid[1:]  # e.g., 01234567 if unid is u1234567
        self.number     = entry['#']  # order in the roll (1 - n-students)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '{},{},{},{}\n'.format( self.unid, self.last_name, self.first_name, self.email)

    def __cmp__(self, other):
        if   self.raw_id < other.raw_id: return -1
        elif self.raw_id > other.raw_id: return  1
        return 0

    def dir_name(self):
        return self.last_name + '_' + self.unid

class ClassRoll:
    """
    ClassRoll provides operations on a CIS-exported roll
    Example usage:
      >>> roll = ClassRoll('ClassRoll.xls')
      >>> roll.export_information( 'roll.csv' )
    """

    def __init__(self, file_n):
        self.src_file_name = file_n
        self.students = []

        if not os.path.isfile(file_n):
            raise Exception('File "{}" does not exist'.format(self.src_file_name))

        self.__load()

    def __iter__(self):
        self.current_student = 0
        return self

    def __next__(self):
        if self.current_student == len( self.students ):
            raise StopIteration
        self.current_student = self.current_student + 1
        return self.students[ self.current_student-1 ]

    def __load(self):
        nheader = 4
        df = pandas.read_excel( self.src_file_name,
                                header=nheader,
                                skiprows=0 )
        df.dropna(inplace=True)

        # split student into first and last names
        name = df['Student'].str.split(',', n=1, expand=True)
        df['Last Name'] = name[0]
        df['First Name'] = name[1]
        df.drop(columns='Student',inplace=True)


        for index, row in df.iterrows():
            # print(row)
            self.students.append( Student(row) )

        # Sanity check: the first student should be numbered "1"
        if ( self.students[0].number != 1 ) :
            raise Exception('Class roll was not in the anticipated format')
        return

    def export_information(self,destFileName):
        '''
        :param destFileName: the name of the csv file to produce (including extension)
        :return: None
        '''
        import csv
        out = open(destFileName,'w')
        for student in self.students:
            out.write(student.__str__())

    def compare_enrollment(self,other):
        print('\nComparing files:\n\t{}\n\t{}'.format(self.src_file_name, other.src_file_name))
        other_ids = list(s.raw_id for s in other.students)
        my_ids    = list(s.raw_id for s in self.students )
        only_other = list( set(other_ids) - set(my_ids   ))
        only_self  = list( set(my_ids   ) - set(other_ids))

        if len(only_other) > 0:
            print('\nThe following students are only in {}\n{:=^50}'.format(other.src_file_name,'') )
            print('{:<18} {:<12} {}\n{:-^50}'.format('Last Name','First Name','UNID',''))
            for id in only_other:
                ix = other_ids.index(id)
                student = other.students[ ix ]
                print('{:<18} {:<12} {}'.format(student.last_name,student.first_name,student.unid))

        if len(only_self) > 0:
            print('\nThe following students are only in {}\n{:=^50}'.format(self.src_file_name,''))
            print('{:<18} {:<12} {}\n{:-^50}'.format('Last Name','First Name','UNID',''))
            for id in only_self:
                ix = my_ids.index(id)
                student = self.students[ ix ]
                print('{:<18} {:<12} {}'.format(student.last_name,student.first_name,student.unid))
            print('{:=^50}'.format(''))

        if len(only_other) == 0 and len(only_self) == 0:
            print('\n** Rolls are identical **\n')

    def dump_student_dirs(self):
        os.mkdir('student_dirs')
        for s in self.students:
            dirname = 'student_dirs/' + s.last_name + '_' + s.unid
            print('Creating dir: "{}"'.format(dirname))
            os.mkdir( dirname )

def convert_roll( srcname, destname ):
    '''
    Converts a CIS-formatted roll to one ready to import directly into a spreadsheet (CSV format)
    :param srcname: the file name for the CIS-formatted roll
    :param destname: the desired file name for the CSV formatted roll
    :return: None
    '''
    ClassRoll(srcname).export_information(destname)
    print('Converted:\n\t{}\nto\n\t{}'.format(srcname,destname))

def compare_enrollment( fname1, fname2 ):
    '''
    Compares two CIS-format rolls and reports differences between them.
    :param fname1: the filename for the first CIS-formatted roll
    :param fname2: the filename for the second CIS-formatted roll
    :return: None
    '''
    ClassRoll(fname1).compare_enrollment( ClassRoll(fname2) )

def create_student_dirs( fname ):
    '''
    Given the CIS-formatted class roll, this generates student directories in the format LASTNAME_UNID
    :param fname: the filename for the CIS-formatted class roll
    :return:
    '''
    roll = ClassRoll(fname)
    roll.dump_student_dirs()

def merge_directories(roll_name, src_path, dest_path):
    '''
    Mergest contents of src_dir into dest_dir using the student information in the supplied roll
    :param roll_name:
    :param src_path:
    :param dest_path:
    :return:
    '''

    import shutil

    roll = ClassRoll( roll_name )
    if not os.path.exists(src_path):
        raise Exception('Source path does not exist:\n\t{}'.format(src_path))
    if not os.path.exists(dest_path):
        os.mkdir(dest_path)

    for student in roll:
        src_dir  = src_path  + '/' + student.dir_name()
        dest_dir = dest_path + '/' + student.dir_name()

        if not os.path.exists(src_dir) :
            print( 'No homework for student {} in folder {}'.format(student.dir_name(), src_path))
            continue

        if not os.path.exists(dest_dir):
            os.mkdir(dest_dir)

        # Look for subdirs of the form HW*_*
        subdirs = os.listdir(src_dir)
        for dir in subdirs:
            if re.match('(HW)([0-9]+)_(\d\d\d\d\d\d\d\d)',dir) == None :
                subdirs.remove(dir)
        subdirs.sort()
        if len(subdirs) < 1:
            print('No homework submission found for student {}'.format(student.dir_name()))
            continue

        # only consider the last submission
        dir = os.path.join(src_dir,subdirs[-1])
        dest_dir = os.path.join( dest_dir, subdirs[-1] )
        # Perform the copy of the files
        files = os.listdir(dir)
        for file in files:
            full_file_name = os.path.join(dir, file)
            if os.path.isfile(full_file_name) :
                # print('\tCopying:\n\t\t{}\n\tto:\n\t\t{}\n'.format(full_file_name,dest_dir))
                if os.path.exists( os.path.join(dest_dir,file)):
                    print('Note: did not copy duplicate file: "{}" for student {}'.format(file,student.dir_name()))
                else:
                    shutil.copy2(full_file_name, dest_dir)
            else:
                print('\tEncountered an unknown file: {}\n'.format(full_file_name))


import tkinter as tk
from tkinter import filedialog

class RollGUI:


    options = [
        [0,'Convert to CSV'],
        [1,'Compare to another roll'],
        [2,'Create student directories'],
        [3,'Merge student directories'],
        [4,'Exit']
    ]

    def doit(self):

        option = self.var.get()
        if option == 4:
            self.master.quit()
            return

        window = tk.Tk('Hello there!')
        window.withdraw()
        window.update()
        self.master.update()
        fnam = tk.filedialog.askopenfilename(title='Select the CIS-formatted roll')
        window.update()
        self.master.update()

        if option == 0:
            convert_roll( fnam, tk.filedialog.asksaveasfilename(title='Choose the file to save',defaultextension='.csv') )
        elif option == 1:
            print('Select the second CIS-formatted roll')
            roll2 = tk.filedialog.askopenfilename(title='Select the CIS-formatted roll to compare to')
            compare_enrollment( fnam, roll2 )
        elif option ==2:
            create_student_dirs(fnam)
        elif option == 3:
            print('Select the source then destination directories in the dialog boxes')
            merge_directories( fnam,
                               tk.filedialog.askdirectory(title='Source directory'),
                               tk.filedialog.askdirectory(title='Destination directory')
                               )
        self.master.quit()
        return

    def __init__(self,master):
        self.master = master
        tk.Label(root,text='What would you like to do?',padx=20,pady=20,font='Helvetica 14 bold').pack()
        self.var = tk.IntVar()
        for option in self.options:
            b = tk.Radiobutton( self.master,
                                text=option[1],
                                val=option[0],
                                variable=self.var,
                                padx=20, pady=2,
                                font='Helvetica 12')
            b.pack(anchor=tk.W)
        tk.Button( self.master, text='OK', command=self.doit ).pack()

if __name__ == '__main__':
    root = tk.Tk()
    gui = RollGUI( root )
    root.mainloop()
    # roll = ClassRoll('Contact List.xls')
    # roll.export_information('roll.txt')


    # convert = str(input('Convert to a CSV format (y/n)? '))
    # if convert.upper() == 'Y':
    #     dest = str(input('\tEnter the destination file name for the CSV-formatted roll: '))
    #     convert_roll(fnam, dest)
    # compare = str(input('Compare with another roll (y/n)? '))
    # if compare.upper() == 'Y':
    #     f2 = str(input('\tEnter the CIS-formatted file to compare to: '))
    #     compare_enrollment(fnam, f2)
    # mkdirs = str(input('Create student directories (y/n)? '))
    # if mkdirs.upper() == 'Y':
    #     create_student_dirs(fnam)
    # merge = str(input('Merge student directories into another folder (y/n)? '))
    # if merge.upper() == 'Y':
    #     p1 = tk.filedialog.askdirectory(title='Source directory')
    #     p2 = tk.filedialog.askdirectory(initialdir=p1, title='Destination directory')
    #
    #     # p1 = str( input('\tEnter the path for the source directory: '))
    #     # p2 = str( input('\tEnter the path for the destination directory: '))
    #     merge_directories(fnam, p1, p2)
