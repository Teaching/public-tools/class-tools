<?php

/*
* PHP COE LDAP/AD User Functions Library
*
* Written by: Tigran Mnatsakanyan and Ryan Taylor
*
*
* This library provides all necessary functions in order
* to perform various manipulations on an ldap/ad user.
*/

// For easy AD manipulation
	require_once("adLDAP.php");

// ChemE AD Cred
	$ChemESuffix 	= '@chemeng.utah.edu';
        $ChemEBase 	= 'DC=CHEMENG,DC=UTAH,DC=EDU';
        $ChemEAdmin 	= 'ADS';
        $ChemEPass 	= 'Gnome8713AD';
	$ChemEAD 	= array("ns3.chemeng.utah.edu");
	
	$UITSuffix 	= '@ad.utah.edu';
	$UITBase   	= 'OU=People,DC=AD,DC=UTAH,DC=EDU';
	$UITAD		= array("ring.ad.utah.edu");

// Definitions
	define("WINDOWS_LENGTH_OF_YEAR",365.2411924319252);
	define("WINDOWS_TIMESTAMP_OFFSET_SECONDS",369 * WINDOWS_LENGTH_OF_YEAR * 24 * 3600);






// ChemE
// Conjunction junction, what's your Function.......
// 

// Mailer
function test_win_to_unix($time)
{
    return ($time / 10000000 - WINDOWS_TIMESTAMP_OFFSET_SECONDS);
}



function test_unix_to_win($time)
{
    return ($time + WINDOWS_TIMESTAMP_OFFSET_SECONDS) * 10000000;
}


function cheme_mailsuccess($firstname,$chemeusername,$email,$unid)
{
        $AllSendTo  = "cbushman@eng.utah.edu, gregorcy@eng.utah.edu, $email";
        $iccsubject = "ChemE ICC Account Information";
        $iccmessage = "
                <html>
                <body>
                        <p> Hi $firstname </p>
                        <p> Your username/password has been created. They are: </p>
                        <p> username: $chemeusername </p>
                        <p> password: Is set to your campus uNid/CIS password </p>
                        <p> Remote access instructions can be found here:http://bitly.com/YYaPlf </p>
                        <p> The ChemE computing policy can be found here: http://bitly.com/1033ZwW </p>
                        <p> Your Ucard will be activated by  Christina Bushman, the Chemical Engineering Executive Secretary</p>
			<p> Your uNid is: $unid </p>";
			
        $iccheaders = 'MIME-Version: 1.0' . "\r\n";
        $iccheaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $iccheaders .= 'From: ChemE Support <support@che.utah.edu>' . "\r\n";

        mail($AllSendTo, $iccsubject, $iccmessage, $iccheaders);
}

function cheme_username($firstname, $lastname)
{
        $first = strtolower($firstname);
        $last = strtolower($lastname);
	
	$chemecombineuser = substr($firstname, 0, 1) . $lastname;
	$chemeusername = strtolower($chemecombineuser); 
		
	return $chemeusername;
}

function cheme_checkusername($chemeusername)
{
	$user = $adldap->user()->infoCollection($chemeusername, array('samaccountname'));
	$nametaken = $user->samaccountname;
	if  (empty($nametaken) == TRUE ) {
                                echo "Nametaken is empty, You can use this username";
                                echo ("<pre>\n");
                                echo $nametaken ;
                                echo ("<pre>\n"); }
                        else {
                                echo "Nametaken is used.  Possible dup accout";
                                echo ("<pre>\n");
                                echo $nametaken;
                                echo ("<pre>\n"); }
}




/**
* Determines which ENG major the student is.
* @param $acadplan academic plan from campus AD.
* @return be, ch, cs, cv, ee, me, or ms
*/
function getAcadMajor($acadplan)
{
	if(contains("Materials Science", $acadplan) || contains("Intmd Material Sci", $acadplan) || contains("Material Science", $acadplan))
		return "ma";
	else if(contains("Computer Science", $acadplan) || contains("Computing", $acadplan))
		return "cs";
	else if(contains("Bioengineering", $acadplan) || contains("Biomedical Engineering", $acadplan))
		return "be";	
	else if(contains("Chemical", $acadplan))
		return "ch"; 
	else if(contains("Computer Engg", $acadplan) || contains("Computer Engineering", $acadplan) || contains("Computatnl Engg", $acadplan))
		return "ce";
	else if(contains("Civil", $acadplan))
		return "cv";
	else if(contains("Electrical", $acadplan))
		return "ee";
	else if(contains("Mechanical", $acadplan))
		return "me";
	else return "coe";
}



?>
