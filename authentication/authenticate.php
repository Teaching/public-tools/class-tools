<?php

function dirToArray($dir) { 
   
   $result = array(); 

   $cdir = scandir($dir); 
   foreach ($cdir as $key => $value) 
   { 
      if (!in_array($value,array(".",".."))) 
      { 
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
         { 
            $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
         } 
         else 
         { 
            $result[] = $value; 
         } 
      } 
   } 
   
   return $result; 
} 

$showfiles_1 = '<html>
<title>Graded Files</title>
<head>
<style type="text/css">
	div.loginBox {
		margin-left: auto;
		margin-right: auto;
		margin-top: auto;
		margin-bottom: auto;
		
		width: 400px;
		min-height: 280px;
		
		border: 1px solid #ccc;
		background-color: #ededed;
		
		border-radius: 3px;
		
		padding: 20px;
		
	}
	div.logins {
		margin-left:auto;
		margin-right:auto;
		width: 280px;
		
	}

	ul.LinkedList { 
		display: block;
		list-style-type: none;
	}
	ul.LinkedList ul { display: none; } 
	.HandCursorStyle { cursor: pointer; cursor: hand; }  /* For IE */
</style>

<script type="text/JavaScript">
    // Add this to the onload event of the BODY element
    function addEvents() {
      activateTree(document.getElementById("LinkedList1"));
    }

    // This function traverses the list and add links 
    // to nested list items
    function activateTree(oList) {
      // Collapse the tree
      for (var i=0; i < oList.getElementsByTagName("ul").length; i++) {
        oList.getElementsByTagName("ul")[i].style.display="none";            
      }                                                                  
      // Add the click-event handler to the list items
      if (oList.addEventListener) {
        oList.addEventListener("click", toggleBranch, false);
      } else if (oList.attachEvent) { // For IE
        oList.attachEvent("onclick", toggleBranch);
      }
      // Make the nested items look like links
      addLinksToBranches(oList);
    }

    // This is the click-event handler
    function toggleBranch(event) {
      var oBranch, cSubBranches;
      if (event.target) {
        oBranch = event.target;
      } else if (event.srcElement) { // For IE
        oBranch = event.srcElement;
      }
      cSubBranches = oBranch.getElementsByTagName("ul");
      if (cSubBranches.length > 0) {
        if (cSubBranches[0].style.display == "block") {
          cSubBranches[0].style.display = "none";
        } else {
          cSubBranches[0].style.display = "block";
        }
      }
    }

    // This function makes nested list items look like links
    function addLinksToBranches(oList) {
      var cBranches = oList.getElementsByTagName("li");
      var i, n, cSubBranches;
      if (cBranches.length > 0) {
        for (i=0, n = cBranches.length; i < n; i++) {
          cSubBranches = cBranches[i].getElementsByTagName("ul");
          if (cSubBranches.length > 0) {
            addLinksToBranches(cSubBranches[0]);
            cBranches[i].className = "HandCursorStyle";
            cBranches[i].style.color = "blue";
            cSubBranches[0].style.color = "black";
            cSubBranches[0].style.cursor = "auto";
          }
        }
      }
    }
  </script>
</head>
<body onload="addEvents();">

<div class="loginBox">
	<div class="logins">';
	
$showfiles_2 = '</div></div></body></html>';

session_start();
require_once("LDAPADFunctions.php");
require_once(dirname(__FILE__) . '/adLDAP.php');

if(isset($_POST['unid']) && $_POST['unid'] != "" && isset($_POST['pass']) && $_POST['pass'] != "") // If we got the information from the form.
{
	$unid = $_POST['unid'] ;
	
    $pass = $_POST['pass'] ;
	try {
		$adldap = new adLDAP(array('account_suffix'=>$UITSuffix,'base_dn'=>$UITBase,  'domain_controllers'=>$UITAD,'admin_username'=>$unid,'admin_password'=>$pass,'use_ssl'=>'true'));
		}	
		catch (adLDAPException $e) {
    			echo $e;
    			exit();
		}
		
	$authUser = $adldap->authenticate($unid, $pass);
	
 
	if ($authUser == true) {
		echo $showfiles_1;
		$user = $adldap->user()->infoCollection($unid, array('*'));
		
		// Get files
		$prepath = getcwd()."/";
		$path = "graded/".strtoupper($user->sn)."_".$user->cn;
		// $prepath .= $path;
		// $files = scandir($prepath);
		// unset($files[0]);
		// unset($files[1]);
		// $files = array_values($files);
		
		$files = dirToArray($prepath.$path);
		
		
		if (count($files) == 0)
			echo "There are no graded file(s) to pick up.";
		else
		{
			
			echo "<h3>Graded Homework</h3>For: ".$user->displayName;
			$i = 0;
			//print_r( $files);
			echo "<hr/><ul id=\"LinkedList1\" class=\"LinkedList\">";
			
			foreach ($files as &$file)
			{
				// echo key($file);
				if (is_array($file))
				{
					$folder = array_search($file, $files, true);
					echo "<li class=\"HandCursorStyle\"><img src=\"images/folder_sm.png\"> ".$folder."<ul style=\"list-style-type: none;\">";
					 
					foreach($file as $sub)
					{
						$filename = (strlen($sub) > 20) ? substr($sub,0,20).'...' : $sub;					
						echo "<li class=\"HandCursorStyle\"><img src=\"images/file_sm.png\"> ".$filename.' <form method="post" action="downloadfile.php" target="_BLANK"><input type="hidden" name="file_location" value="'.$prepath.$path."/".$folder."/".$sub.'"><input type="submit" value="Download"></form></li>';
					}					 
					echo "</ul></li>";
				}
				else
				{
					$filename = (strlen($file) > 20) ? substr($file,0,20).'...' : $file;					
					echo "<li><img src=\"images/file_sm.png\"> ".$filename.' <form method="post" action="downloadfile.php" target="_BLANK"><input type="hidden" name="file_location" value="'.$prepath.$path."/".$file.'"><input type="submit" value="Download"></form></li>';
					
				}
				$i++;
			}
			
			echo "</ul>";
		}
		 
		// Close connection to OIT AD
		$adldap->close(); 
		echo $showfiles_2;
	}
	else { 
		header("Location: http://www.che.utah.edu/~sutherland");
	}
	
	$adldap->close();
}
else {
	$adldap->close(); 
	echo "User authentication failed ";
}

?>	
