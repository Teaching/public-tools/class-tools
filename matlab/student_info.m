function [lastname,firstname,id,umail,level] = student_info( fname, format )
% [idtxt,name] = student_info( fname, format )
%
% INPUTS:
%   fname - the file name that contains unid and last names.  The format is
%           the format of the tab-delimited roll obtained directly from
%           CIS.
%   format - [OPTIONAL] available options are 'chemeng' or 'unid'
%            (default).  chemeng option results in a 0 prefix while unid
%            results in a u prefix.
%
% OUPUTS:
%   lastname  - a cell array of student last  names.
%   firstname - a cell array of student first names.
%   id        - a cell array of strings of the student ids
%   umail
%

nheader = 8;

if( nargin==1 ) format='unid'; end

[rank,lastname,firstname,junk,id,junk,level] = textread( ...
   fname,...
   '%u %s %s %s %s %f %s',...
   'delimiter','\t,',...
   'headerlines',nheader );

switch lower(format) 
   case 'unid'
      id = char(id);
      id(:,1) = 'u';
      id = cellstr(id);
   case 'chemeng'
   otherwise
      error('invalid formatting option.');
end

umail = char(id);
umail(:,1) = 'u';
umail = cellstr(id);
for i=1:length(id)
  umail{i} = strcat(umail{i},'@utah.edu');
end

% sanity check.  If we eat up too many headers or have trouble reading,
% this should fail.
assert( rank(1) == 1 );
assert( rank(length(rank)) == length(rank) );

% fprintf('According to the information in "%s,"\n',fname);
% fprintf('There are %1.0f students enrolled\n',length(junk));
