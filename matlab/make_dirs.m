function make_dirs(fname)
% make_dirs(fname)
%
% creates directories in the form "lastname_unid"
%
% INPUTS:
%   fname - the name of the file containing the CIS class role.
%

if nargin==0
  [fname,p] = uigetfile('*','Select class roll');
  fname = strcat(p,fname);
  clear p;
end

[name,first,id] = student_info(fname,'unid');
path = 'studentDirs';
mkdir(path);

for( i=1:length(id) )
   dirname = strcat( name{i}, '_', id{i} );
   fprintf('making directory: %s\n',dirname);
   mkdir( strcat('./',path,'/',dirname) );
end