function compare_enrollment( file1, file2 )
% compare_enrollment( file1, file2 )
%
% Compares enrollment in file1 with that in file2.
% Unique entries in each file are printed to the screen.
%
if nargin==0
  [file1,path1] = uigetfile('*','Select first class roll');
  [file2,path2] = uigetfile('*','Select second class roll');
  file1 = strcat(path1,file1);
  file2 = strcat(path2,file2);
end

[last1,first1,id1] = student_info(file1,'chemeng');
[last2,first2,id2] = student_info(file2,'chemeng');

id1 = char(id1);
id2 = char(id2);

% convert ids to numbers
id1num = str2num( id1 );
id2num = str2num( id2 );

diff1 = setdiff(id1num,id2num);
diff2 = setdiff(id2num,id1num);

if( ~isempty(diff1) )
   print_entries( file1, diff1, id1num, last1, first1 );
end

if( ~isempty(diff2) )
   print_entries( file2, diff2, id2num, last2, first2 );
end

if( isempty(diff1) && isempty(diff2) )
   fprintf('\n*** class rolls are identical ***\n\n');
end


end

%====================================================================

function print_entries( fnam, idnum, idset, lastn, firstn )
fprintf('\nEntries unique to "%s"\n',fnam);
fprintf('%8s\t%8s\t%s\t%s\n','ChEn ID','UNID','Last','First Middle');
fprintf('-----------------------------------------------------\n');
% find the id
for( i=1:length(idnum) )
   id = idnum(i);
   ix = find( id == idset );
   % convert the id from a number to a valid id
   myid = num2str(id);
   nfill = 7 - length(myid);
   for( j=1:nfill )
      myid = strcat('0',myid);
   end

   fprintf('%s\t%s\t%s\t%s\n',...
      strcat('0',myid),...
      strcat('u',myid),...
      lastn{ix},...
      firstn{ix} );
end
fprintf('-----------------------------------------------------\n');
end