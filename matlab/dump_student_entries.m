function dump_student_entries( fname, outfname )
%dump_student_enries( fname, outfname )
%
% Loads information from the CIS formatted role and writes the student's
% level, last name, first name, and UNID to a tab-delimited file.
%
% INPUT:
%  fname    - the name of the file containing the CIS formatted role
%  outfname - the name of the file to write the info to.
%

if nargin==0
  [fname,p] = uigetfile('*','Select the class roll');
  fname = strcat(p,fname);
  clear p;
end

if nargin<2
  [outfname,p,ix] = uiputfile('*','Select name for output file');
  if ix==0
    outfname='';
  else
    outfname = strcat(p,outfname);
  end
  clear p;
end
  

[last,first,id,email,level] = student_info(fname,'unid');

if length(outfname)==0
  fid = 1;
else
  fid = fopen(outfname,'w');
end

for( i=1:length(last) )
   fprintf(fid,'%s\t%s\t%s\t%s\t%s\n',...
   level{i}, id{i}, last{i}, first{i}, email{i} );
end

if fid>1
  fclose( fid );
end

fprintf('\nAccording to the information in "%s"\n',fname);
fprintf('  there are %1.0f students enrolled\n',length(last));
fprintf('\nStudent information written to tab-delimited file "%s"\n\n',outfname);