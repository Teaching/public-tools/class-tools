function dirMerge( rollName, srcDir, destDir )
% dirMerge( rollName )
%
% Queries for source and destination paths.  Copies all information from
% the student folders in the source path to those in the destination path.
%
% INPUTS:
%   rollName : the file containing the class roll entries (CIS format)
%

if( nargin==0 )
  [rollName,path1] = uigetfile('*','Select the class roll');
  rollName = strcat(path1,rollName);
end

[lastName,firstName,unid] = student_info( rollName );


if( nargin<2 )
  srcDir  = uigetdir(path1,'select the source dir');
end
if( nargin<3 )
  destDir = uigetdir(path1,'select the dest dir');
end

n = length(unid);
for( i=1:n )
  id = strcat( lastName{i}, '_', unid{i} );
  
  dir = strcat('"',srcDir,'/',id,'"');
  [ii,subdirs] = system( strcat(['ls ',dir]) );
  if( isempty(subdirs) )
    fprintf('No homework submitted for student: %s in folder %s\n',id,srcDir);
    continue;
  end
  hwdirs = sort( regexp( subdirs, '(?:HW)\d\w{0,1}_([0-9]+)','match') );
  if length(hwdirs) == 0 
    fprintf('No homework submission found for student %25s\n',id);
    continue
  end
  hwdir = hwdirs{end};
  if( isempty(hwdir) )
    fprintf('Problems copying files for student: %25s - no submission?\n',id);
    continue;
  end
  
  d = strcat(destDir,'/',id);
if( ~isdir(d) )
  status=system(strcat(['mkdir "',d,'"']));
  assert( status==0 );
end
  
  s = strcat(srcDir,'/',id,'/',hwdir);
  d = strcat(destDir,'/',id,'/',hwdir);
  if( ~isdir(d) )
    status=system(strcat(['mkdir "',d,'"']));
    if( status~=0 )
      fprintf('problems with directory creation for %s/%s\n',destDir,id);
    end
  end
  
  % deal with possible spaces in directory names - escape them with "\" so
  % that the copy works properly.
  d = strrep(d,' ','\ ');
  s = strrep(s,' ','\ ');
  cmd = strcat(['cp ',s,'/* ',d,'/.']);
  [status,result] = system(cmd);
  if( status~=0 )
    fprintf('Problems copying files for student: %25s - no submission?\n',id);
  end
  
end